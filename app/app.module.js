// Define the `goldOwlApp` module
angular.module('goldOwlApp', [
    // ...which depends on the `phoneList` module
    'ngRoute',
    'core',
    'phoneList',
    'phoneDetail',
    'ngAnimate'
]);
