describe('phoneList', function () {


    // Load the module that contains the `phoneList` component before each test
    beforeEach(module('phoneList'));

    // Test the controller
    describe('PhoneListController', function () {
      var $httpBackend, ctrl;
      var phonesData = [{name: 'Nexus S'}, {name: 'Motorola Droid'}];

      beforeEach(function(){
        jasmine.addCustomEqualityTester(angular.equals);
      });

      // The injector ingores leading and trailing underscores here (i.e. _$httpBackend_).
      // This allows us to inject a service and assign it to a variable with the same name
      // as the service while avoiding a name conflit.
      beforeEach(inject(function($componentController, _$httpBackend_){
        $httpBackend = _$httpBackend_;
        $httpBackend.expectGET('phones/phones.json').respond(phonesData);
        ctrl = $componentController('phoneList');
      }));

      it('should create a `phones` model with 2 phones fetched with `$http`', function() {
          expect(ctrl.phones).toEqual([]);
          $httpBackend.flush();
          expect(ctrl.phones).toEqual(phonesData);
      });

      it('should set a default value for the `orderProp` model', function(){
         expect(ctrl.orderProp).toBe('age');
      });
    });
});
