'use strict';

describe('phoneDetail', function(){



  // Load the module that contains the `phoneDetail` component before each test.
  beforeEach(module('phoneDetail'));

  // Test the controller
  describe('PhoneDetailController', function(){
    var $httpBackend, ctrl;
    var phoneData = {
          name: 'phone A',
          images: ['image/url1.png', 'image/url2.png']
        };

    beforeEach(function(){
        jasmine.addCustomEqualityTester(angular.equals);
    });

    beforeEach(inject(function($componentController, _$httpBackend_, $routeParams){
      $httpBackend = _$httpBackend_;
      $httpBackend.expectGET('phones/a.json').respond(phoneData);
      $routeParams.phoneId = 'a';
      ctrl = $componentController('phoneDetail');
    }));

    it('should fetch the phone details', function(){
      expect(ctrl.phone).toEqual({});
      $httpBackend.flush();
      expect(ctrl.phone).toEqual(phoneData);
    });

  });
});
